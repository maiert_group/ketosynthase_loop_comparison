#!/bin/bash

curl --header "Content-Type: application/json" --data '{"query":{"terms":{"term_type":"op","operation":"AND","left":{"term_type":"expr","category":"type","term":"t1pks"},"right":{"term_type":"expr","category":"asdomain","term":"PKS_KS"}},"search":"domain","return_type":"csv"},"offset":0} 'https://antismash-db.secondarymetabolites.org/api/v1.0/export > KS_t1.csv

curl --header "Content-Type: application/json" --data '{"query":{"terms":{"term_type":"op","operation":"AND","left":{"term_type":"expr","category":"asdomain","term":"PKS_KS"},"right":{"term_type":"op","operation":"AND","left":{"term_type":"expr","category":"type","term":"t1pks"},"right":{"term_type":"expr","category":"type","term":"transat-pks"}}},"search":"domain","return_type":"csv"},"offset":0}' https://antismash-db.secondarymetabolites.org/api/v1.0/export > KS_t1_trans.csv
